const routes = {
  login: "/auth/login",
  users: "/app/customers/users",
  user_details: '/app/customers/user-details',
  guarantor: '/app/customers/guarantor',
  organization: '/app/business/organization',
  loans: '/app/customers/loans',
  decision_models: '/app/customers/decision-models',
  savings: '/app/customers/savings',
  loan_request: '/app/customers/loan-request',
  whitelist: '/app/customers/whitelist',
  karma: '/app/customers/karma',
};

export const app_routes = [routes.users, routes.user_details];

export default routes;
