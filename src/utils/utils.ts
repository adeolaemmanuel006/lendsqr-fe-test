import { useMediaQuery } from "react-responsive";

const STORED_DATA: StorageData = {
  accountBalance: "",
  createdAt: "",
  education: {
    duration: "",
    employmentStatus: "",
    level: "",
    loanRepayment: "",
    monthlyIncome: [],
    officeEmail: "",
    sector: "",
  },
  email: "",
  guarantor: {
    address: "",
    firstName: "",
    gender: "",
    lastName: "",
    phoneNumber: "",
  },
  accountNumber: "",
  id: "",
  lastActiveDate: "",
  orgName: "",
  phoneNumber: "",
  profile: {
    address: "",
    avatar: "",
    bvn: "",
    currency: "NGN",
    firstName: "",
    gender: "",
    lastName: "",
    phoneNumber: "",
  },
  socials: {
    facebook: "",
    instagram: "",
    twitter: "",
  },
  userName: "",
};

/**
 * Concatenates an array of string and return a single string
 * @param data
 * @returns String
 */
export const concatClassName = (data: string[]) => {
  let string = "";
  data.forEach((str, ind) => {
    if (ind === 0) string = string.concat(str);
    else string = string.concat(` ${str}`);
  });
  return string;
};

export function useGetScreen() {
  return {
    isMobilePortrait: useMediaQuery({
      query: `(min-width: 320px) and (max-width: 480px)`,
    }),
    isMobileLandscape: useMediaQuery({
      query: `(min-width: 481px) and (max-width: 767px)`,
    }),
    isMediumPortrait: useMediaQuery({
      query: `(min-width: 768px) and (max-width: 1024px)`,
    }),
    isMediumLandscape: useMediaQuery({
      query: `(min-width: 768px) and (max-width: 1024px) and (orientation: landscape)`,
    }),
    isLargeDesktop: useMediaQuery({
      query: `(min-width: 1025px) and (max-width: 1280px)`,
    }),
    isXLargeDesktop: useMediaQuery({ query: `(min-width: 1281px)` }),
  };
}

/**
 * Function that sets localStorage
 * @param payload `Stored data`
 */
export function storeData(payload: StoreObject) {
  const store = localStorage as Storage;
  Object.keys(payload).forEach((key) => {
    store.setItem(key, JSON.stringify(payload[key]));
  });
}

/**
 * Function called to get stored data from localStorage
 * @returns `Stored data`
 */
export function getStoredData(): any {
  const storage = localStorage as Storage;
  const data = Object.keys(STORED_DATA).reduce((prev, acc) => {
    return {
      ...prev,
      [acc]: JSON.parse(storage.getItem(acc) as string),
    };
  }, {});
  return data;
}
