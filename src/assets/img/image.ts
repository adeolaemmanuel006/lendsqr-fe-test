import auth_image from "./auth-bg.svg";
import logo from "./logo.svg";
import logoText from "./logo-text.svg";
import briefcase from "./briefcase.svg";
import home from "./home.svg";
import user from "./user.svg";
import users_2 from "./users_2.svg";
import sack from "./sack.svg";
import handshake from "./handshake.svg";
import piggy_bank from "./piggy-bank.svg";
import loan_request from "./loan_request.svg";
import user_check from "./user_check.svg";
import user_times from "./user_times.svg";
import savings from "./savings.svg";
import coins_solid from "./coins_solid.svg";
import transaction from "./transaction.svg";
import galaxy from "./galaxy.svg";
import user_cog from "./user_cog.svg";
import scroll from "./scroll.svg";
import chart_bar from "./chart_bar.svg";
import sliders from "./sliders.svg";
import badge_percent from "./badge_percent.svg";
import clipboard_list from "./clipboard_list.svg";
import colored_users from "./colored_users.svg";
import colored_users_2 from "./colored_users_2.svg";
import trans_coin from "./trans_coin.svg";
import colored_coin from "./colored_coin.svg";
import avatar from "./avatar.svg";
import start_outlined from "./start_outlined.svg";
import star from "./star.svg";
import user_avatar from "./user_avatar.png";

const icon = {
  auth_image,
  logo,
  logoText,
  briefcase,
  home,
  user,
  users_2,
  sack,
  handshake,
  piggy_bank,
  loan_request,
  user_check,
  user_times,
  savings,
  coins_solid,
  transaction,
  galaxy,
  user_cog,
  scroll,
  chart_bar,
  sliders,
  badge_percent,
  clipboard_list,
  colored_users,
  colored_users_2,
  trans_coin,
  colored_coin,
  avatar,
  start_outlined,
  star,
  user_avatar
};
export default icon;
