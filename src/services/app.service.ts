import HttpClient from "./http.service";

class AppServices extends HttpClient {
  public getAllUsers = async () => {
    try {
      const res = await this.get<UserDto[] | undefined>("users");
      return res.data;
    } catch (error) {
      console.log(error);
    }
  };

  public getUserById = async (id: string) => {
    try {
        const res = await this.get<any>(`users/${id}`);
        return res.data;
      } catch (error) {
        console.log(error);
      }
  };
}

const appServices = new AppServices({});
export default appServices;
