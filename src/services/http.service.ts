import axios, {
  AxiosInstance,
  AxiosRequestConfig,
  AxiosResponse,
  CreateAxiosDefaults,
} from "axios";

class HttpClient {
  protected readonly instance: AxiosInstance;
  constructor(config: CreateAxiosDefaults) {
    config.baseURL =
      "https://6270020422c706a0ae70b72c.mockapi.io/lendsqr/api/v1/";
    this.instance = axios.create(config);
  }

  _interceptors() {
    this.instance.interceptors.request.use();
    this.instance.interceptors.response.use();
  }

  protected async post<T>(
    url: string,
    data: any,
    config?: AxiosRequestConfig<any> | undefined
  ): Promise<AxiosResponse<T>> {
    return await this.instance.post(url, data, config);
  }

  protected async get<T>(
    url: string,
    config?: AxiosRequestConfig<any> | undefined
  ): Promise<AxiosResponse<T>> {
    return await this.instance.get(url, config);
  }
}

export default HttpClient;
