import React from "react";
import {
  BrowserRouter as Router,
  Routes,
  Route,
  Navigate,
} from "react-router-dom";
import Layout from "./components/layout";
import UserDetails from "./container/app/customers/userDetails";
import Users from "./container/app/customers/users";
import Login from "./container/auth/login";
import routes from "./utils/routes";

const App: React.FC = () => {
  return (
    <Router>
      <Layout>
        <Routes>
          <Route path="/" element={<Navigate to={routes.login} />} />
          <Route path={routes.login} element={<Login />} />
          <Route path={routes.users} element={<Users />} />
          <Route path={routes.user_details} element={<UserDetails />} />
        </Routes>
      </Layout>
    </Router>
  );
};
export default App;
