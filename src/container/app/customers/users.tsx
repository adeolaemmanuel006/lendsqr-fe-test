import { concatClassName, useGetScreen } from "../../../utils/utils";
import styles from "../../../scss/containers/customers.module.scss";
import icon from "../../../assets/img/image";
import { createColumnHelper } from "@tanstack/react-table";
import Table from "../../../components/table/table";
import { BsFilter } from "react-icons/bs";
import { AiOutlineEye } from "react-icons/ai";
import { FiUserCheck } from "react-icons/fi";
import { FiUserX } from "react-icons/fi";
import React from "react";
import More from "../../../components/more";
import routes from "../../../utils/routes";
import { useNavigate } from "react-router-dom";
import appServices from "../../../services/app.service";
import moment from "moment";
import StatusPill from "../../../components/table/statusPill";

const Users: React.FC<UserProps> = () => {
  const navigate = useNavigate();
  const { isMobilePortrait } = useGetScreen();
  const [users, setUsers] = React.useState<Partial<UserColumnDto[]>>([]);
  const [showFilter, setShowFilter] = React.useState(false);
  const columnHelper = createColumnHelper<UserColumnDto>();

  React.useEffect(() => {
    getUsersTable();
    // eslint-disable-next-line
  }, []);

  async function getUsersTable() {
    const res = await appServices.getAllUsers();
    if (res) {
      const data = res.map(
        ({ orgName, userName, email, phoneNumber, createdAt, id }) => {
          return {
            ORGANIZATION: orgName,
            USERNAME: userName,
            EMAIL: email,
            "PHONE NUMBER": phoneNumber,
            "DATE JOINED": moment(createdAt).toISOString(),
            STATUS: "Active",
            id,
          };
        }
      );
      setUsers(data);
    }
  }

  const column = [
    columnHelper.accessor((row) => row["ORGANIZATION"], {
      id: "organization",
      cell: (info) => <span className="fs-6">{info.getValue()}</span>,
      header: () => (
        <div className="flex" onClick={() => setShowFilter(!showFilter)}>
          <p>ORGANIZATION</p>
          <BsFilter size={23} />
        </div>
      ),
    }),
    columnHelper.accessor((row) => row["USERNAME"], {
      id: "username",
      cell: (info) => <span className="fs-6">{info.getValue()}</span>,
      header: () => (
        <div className="flex" onClick={() => setShowFilter(!showFilter)}>
          <p>USERNAME</p>
          <BsFilter size={23} />
        </div>
      ),
    }),
    columnHelper.accessor((row) => row["EMAIL"], {
      id: "email",
      cell: (info) => <span className="fs-6">{info.getValue()}</span>,
      header: () => (
        <div className="flex" onClick={() => setShowFilter(!showFilter)}>
          <p>EMAIL</p>
          <BsFilter size={23} />
        </div>
      ),
    }),
    columnHelper.accessor((row) => row["PHONE NUMBER"], {
      id: "PHONE-NUMBER",
      cell: (info) => <span className="fs-6">{info.getValue()}</span>,
      header: () => (
        <div className="flex" onClick={() => setShowFilter(!showFilter)}>
          <p>PHONE NUMBER</p>
          <BsFilter size={23} />
        </div>
      ),
    }),
    columnHelper.accessor((row) => row["DATE JOINED"], {
      id: "DATE-JOINED",
      cell: (info) => <span className="fs-6">{info.getValue()}</span>,
      header: () => (
        <div className="flex" onClick={() => setShowFilter(!showFilter)}>
          <p>DATE JOINED</p>
          <BsFilter size={23} />
        </div>
      ),
    }),
    columnHelper.accessor((row) => row["STATUS"], {
      id: "status",
      cell: (info) => <StatusPill type={info.getValue()} />,
      header: () => (
        <div className="flex" onClick={() => setShowFilter(!showFilter)}>
          <p>STATUS</p>
          <BsFilter size={23} />
        </div>
      ),
    }),
    columnHelper.accessor((row) => row["id"], {
      id: "id",
      cell: (row) => (
        <More
          more={[
            {
              label: "View Details",
              icon: <AiOutlineEye size={25} />,
              action: () =>
                navigate?.(routes.user_details, {
                  state: { id: row.getValue() },
                }),
            },
            {
              label: "Blacklist User",
              icon: <FiUserX size={25} />,
            },
            {
              label: "Activate User",
              icon: <FiUserCheck size={25} />,
            },
          ]}
        />
      ),
      header: () => "",
    }),
  ];

  const column_mobile = [
    columnHelper.accessor((row) => row["ORGANIZATION"], {
      id: "organization",
      cell: (info) => <span className="fs-6">{info.getValue()}</span>,
      header: () => (
        <div className="flex" onClick={() => setShowFilter(!showFilter)}>
          <p>ORGANIZATION</p>
          <BsFilter size={23} />
        </div>
      ),
    }),
    columnHelper.accessor((row) => row["STATUS"], {
      id: "status",
      cell: (info) => <StatusPill type={info.getValue()} />,
      header: () => (
        <div className="flex" onClick={() => setShowFilter(!showFilter)}>
          <p>STATUS</p>
          <BsFilter size={23} />
        </div>
      ),
    }),
    columnHelper.accessor((row) => row["id"], {
      id: "id",
      cell: (row) => (
        <More
          more={[
            {
              label: "View Details",
              icon: <AiOutlineEye size={25} />,
              action: () =>
                navigate?.(routes.user_details, {
                  state: { id: row.getValue() },
                }),
            },
            {
              label: "Blacklist User",
              icon: <FiUserX size={25} />,
            },
            {
              label: "Activate User",
              icon: <FiUserCheck size={25} />,
            },
          ]}
        />
      ),
      header: () => "",
    }),
  ];

  const statistics = [
    { icon: icon.colored_users, label: "Users", count: "2,344" },
    { icon: icon.colored_users_2, label: "Active Users", count: "2,344" },
    { icon: icon.trans_coin, label: "Users with Loans", count: "12,453" },
    { icon: icon.colored_coin, label: "Users with Savings", count: "102,453" },
  ];

  return (
    <div className={concatClassName([styles.user_container])}>
      <h1 className={concatClassName([styles.page_name])}>Users</h1>

      <div className={concatClassName([styles.main])}>
        <div className={concatClassName([styles.statistics])}>
          {statistics.map((data, key) => {
            return (
              <div className={concatClassName([styles.modal])} key={key}>
                <img
                  src={data.icon}
                  alt={data.label}
                  className={concatClassName([styles.img])}
                />
                <p className={concatClassName([styles.label])}>{data.label}</p>
                <p className={concatClassName([styles.count])}>{data.count}</p>
              </div>
            );
          })}
        </div>
      </div>

      <div className={concatClassName([styles.table_con])}>
        <Table
          columns={isMobilePortrait ? column_mobile : column}
          data={users}
          filterShow={showFilter}
        />
      </div>
    </div>
  );
};
export default Users;
