import React from "react";
import { concatClassName, storeData } from "../../../utils/utils";
import styles from "../../../scss/containers/customers.module.scss";
import { HiArrowLongLeft } from "react-icons/hi2";
import icon from "../../../assets/img/image";
import { useLocation, useNavigate } from "react-router-dom";
import appServices from "../../../services/app.service";

const UserDetails: React.FC<UserDetailsProps> = () => {
  const [details, setDetails] = React.useState<UserDto>();
  const location = useLocation();
  const navigate = useNavigate();

  React.useEffect(() => {
    getUser();
    // eslint-disable-next-line
  }, []);

  async function getUser() {
    const res = await appServices.getUserById(location.state.id);
    if (res) {
      storeData(res);
      setDetails(res);
    }
  }

  const tab = [
    { label: "General Details", active: true },
    { label: "Documents", active: false },
    { label: "Bank Details", active: false },
    { label: "Loans", active: false },
    { label: "Savings", active: false },
    { label: "App and System", active: false },
  ];

  return (
    <div className={concatClassName([styles.user_details_container])}>
      <div
        onClick={() => navigate(-1)}
        className={concatClassName([styles.back])}
      >
        <HiArrowLongLeft onClick={() => navigate(-1)} size={25} />
        <p>Back to Users</p>
      </div>

      <div className={concatClassName([styles.page_con])}>
        <h1 className={concatClassName([styles.page_name])}>User Details</h1>
        <div className={concatClassName([styles.btn_grp])}>
          <button className={concatClassName(["border_btn_red", styles.btn])}>
            Blacklist User
          </button>
          <button className={concatClassName(["border_btn_green", styles.btn])}>
            Activate User
          </button>
        </div>
      </div>

      <div className={concatClassName([styles.main])}>
        <div className={concatClassName([styles.user_con])}>
          <div className={concatClassName([styles.user])}>
            <img
              src={details?.profile.avatar}
              alt="avatar"
              className={concatClassName([styles.avatar])}
            />
            <div className={concatClassName([styles.text_con])}>
              <p className="theme-text fw-500">
                {details?.profile.firstName} {details?.profile.lastName}
              </p>
              <p className="theme-text_2 fs-6">{details?.userName}</p>
            </div>
            <div className={concatClassName([styles.text_con])}>
              <p className="theme-text_2">User’s Tier</p>
              <div className="flex gap-2">
                <img src={icon.star} alt="star" />
                <img src={icon.start_outlined} alt="star" />
                <img src={icon.start_outlined} alt="star" />
              </div>
            </div>
            <div className={concatClassName([styles.text_con])}>
              <p className="theme-text_2 fw-500">₦{details?.accountBalance}</p>
              <p className="fs-1">{details?.accountNumber}/Providus Bank</p>
            </div>
          </div>

          <div className={concatClassName([styles.tab_con])}>
            {tab.map(({ label, active }, id) => {
              if (active)
                return (
                  <div
                    key={id}
                    className={concatClassName([styles.tab_active])}
                  >
                    {label}
                  </div>
                );
              else
                return (
                  <div className={concatClassName([styles.tab])}>{label}</div>
                );
            })}
          </div>
        </div>

        <div className={concatClassName([styles.user_details])}>
          <div className={concatClassName([styles.details_con])}>
            <h4 className={concatClassName([styles.title])}>
              Personal Information
            </h4>
            <div className={concatClassName([styles.details])}>
              <div>
                <h6 className={concatClassName([styles.header])}>full Name</h6>
                <p className={concatClassName([styles.text])}>
                  {" "}
                  {details?.profile.firstName} {details?.profile.lastName}
                </p>
              </div>
              <div>
                <h6 className={concatClassName([styles.header])}>
                  Phone Number
                </h6>
                <p className={concatClassName([styles.text])}>
                  {details?.profile.phoneNumber}
                </p>
              </div>
              <div>
                <h6 className={concatClassName([styles.header])}>
                  Email Address
                </h6>
                <p className={concatClassName([styles.text])}>
                  {details?.email}
                </p>
              </div>
              <div>
                <h6 className={concatClassName([styles.header])}>Bvn</h6>
                <p className={concatClassName([styles.text])}>
                  {details?.profile.bvn}
                </p>
              </div>
              <div>
                <h6 className={concatClassName([styles.header])}>Gender</h6>
                <p className={concatClassName([styles.text])}>
                  {" "}
                  {details?.profile.gender}
                </p>
              </div>
            </div>
            <div className={concatClassName([styles.details, "mt-3"])}>
              <div>
                <h6 className={concatClassName([styles.header])}>
                  Marital status
                </h6>
                <p className={concatClassName([styles.text])}>
                  {" "}
                  {details?.profile.phoneNumber}
                </p>
              </div>
              <div>
                <h6 className={concatClassName([styles.header])}>Children</h6>
                <p className={concatClassName([styles.text])}>None</p>
              </div>
              <div>
                <h6 className={concatClassName([styles.header])}>
                  Type of residence
                </h6>
                <p className={concatClassName([styles.text])}>
                  Parent’s Apartment
                </p>
              </div>
            </div>
          </div>

          <div className={concatClassName([styles.details_con, "mt-4"])}>
            <h4 className={concatClassName([styles.title])}>
              Education and Employment
            </h4>
            <div className={concatClassName([styles.details])}>
              <div>
                <h6 className={concatClassName([styles.header])}>
                  level of education
                </h6>
                <p className={concatClassName([styles.text])}>
                  {details?.education.level}
                </p>
              </div>
              <div>
                <h6 className={concatClassName([styles.header])}>
                  employment status
                </h6>
                <p className={concatClassName([styles.text])}>
                  {details?.education.employmentStatus}
                </p>
              </div>
              <div>
                <h6 className={concatClassName([styles.header])}>
                  sector of employment
                </h6>
                <p className={concatClassName([styles.text])}>
                  {details?.education.sector}
                </p>
              </div>
              <div>
                <h6 className={concatClassName([styles.header])}>
                  Duration of employment
                </h6>
                <p className={concatClassName([styles.text])}>
                  {details?.education.duration}
                </p>
              </div>
            </div>
            <div className={concatClassName([styles.details_2, "mt-3"])}>
              <div>
                <h6 className={concatClassName([styles.header])}>
                  office email
                </h6>
                <p className={concatClassName([styles.text])}>
                  {details?.education.officeEmail}
                </p>
              </div>
              <div>
                <h6 className={concatClassName([styles.header])}>
                  Monthly income
                </h6>
                <p className={concatClassName([styles.text])}>
                  {details?.education.monthlyIncome}
                </p>
              </div>
              <div>
                <h6 className={concatClassName([styles.header])}>
                  loan repayment
                </h6>
                <p className={concatClassName([styles.text])}>
                  {details?.education.loanRepayment}
                </p>
              </div>
            </div>
          </div>

          <div className={concatClassName([styles.details_con, "mt-4"])}>
            <h4 className={concatClassName([styles.title])}>Socials</h4>
            <div className={concatClassName([styles.details])}>
              <div>
                <h6 className={concatClassName([styles.header])}>Twitter</h6>
                <p className={concatClassName([styles.text])}>
                  {details?.socials.twitter}
                </p>
              </div>
              <div>
                <h6 className={concatClassName([styles.header])}>Facebook</h6>
                <p className={concatClassName([styles.text])}>
                  {details?.socials.facebook}
                </p>
              </div>
              <div>
                <h6 className={concatClassName([styles.header])}>Instagram</h6>
                <p className={concatClassName([styles.text])}>
                  {details?.socials.instagram}
                </p>
              </div>
            </div>
          </div>

          <div className={concatClassName([styles.details_con, "mt-4"])}>
            <h4 className={concatClassName([styles.title])}>Guarantor</h4>
            <div className={concatClassName([styles.details])}>
              <div>
                <h6 className={concatClassName([styles.header])}>full Name</h6>
                <p className={concatClassName([styles.text])}>
                  {details?.guarantor.firstName} {details?.guarantor.lastName}
                </p>
              </div>
              <div>
                <h6 className={concatClassName([styles.header])}>
                  Phone Number
                </h6>
                <p className={concatClassName([styles.text])}>
                  {details?.guarantor.phoneNumber}
                </p>
              </div>
              <div>
                <h6 className={concatClassName([styles.header])}>
                  Email Address
                </h6>
                <p className={concatClassName([styles.text])}>
                  debby@gmail.com
                </p>
              </div>
              <div>
                <h6 className={concatClassName([styles.header])}>
                  Relationship
                </h6>
                <p className={concatClassName([styles.text])}>Sister</p>
              </div>
            </div>
          </div>

          <div className={concatClassName([styles.details_con, "mt-4"])}>
            <div className={concatClassName([styles.details])}>
              <div>
                <h6 className={concatClassName([styles.header])}>full Name</h6>
                <p className={concatClassName([styles.text])}>Debby Ogana</p>
              </div>
              <div>
                <h6 className={concatClassName([styles.header])}>
                  Phone Number
                </h6>
                <p className={concatClassName([styles.text])}>07060780922</p>
              </div>
              <div>
                <h6 className={concatClassName([styles.header])}>
                  Email Address
                </h6>
                <p className={concatClassName([styles.text])}>
                  debby@gmail.com
                </p>
              </div>
              <div>
                <h6 className={concatClassName([styles.header])}>
                  Relationship
                </h6>
                <p className={concatClassName([styles.text])}>Sister</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default UserDetails;
