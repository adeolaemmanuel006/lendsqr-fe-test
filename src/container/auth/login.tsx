import React from "react";
import { Link } from "react-router-dom";
import icon from "../../assets/img/image";
import Button from "../../components/button";
import Input from "../../components/input";
import Logo from "../../components/logo";
import styles from "../../scss/auth.module.scss";
import routes from "../../utils/routes";
import { concatClassName } from "../../utils/utils";

const Login: React.FC<LoginProps> = () => {
  return (
    <div className={styles.container}>
      <div className={styles.half}>
        <div className={styles.logo_con}>
          <Logo />
        </div>
        <div className={styles.image_container}>
          <img src={icon.auth_image} alt="login" className={styles.img_login} />
        </div>
      </div>
      <div className={concatClassName([styles.half, styles.login_form_con])}>
        <form>
          <h1 className={concatClassName([styles.header])}>Welcome!</h1>
          <p className={concatClassName([styles.sub_header])}>
            Enter details to login.
          </p>
          <div className={styles.input_con}>
            <Input type={"text"} placeholder="Email" className={styles.input} />
            <Input
              type={"password"}
              placeholder="Password"
              className={styles.input}
            />
            <Link to={""} className={styles.forgot}>
              Forgot PASSWORD?
            </Link>

            <Button
              to={routes.users}
              className={concatClassName([styles.login_btn])}
            >
              LOG IN
            </Button>
          </div>
        </form>
      </div>
    </div>
  );
};
export default Login;
