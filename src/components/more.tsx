import React from "react";
import { IoIosMore } from "react-icons/io";
import styles from "../scss/component.module.scss";
import { concatClassName } from "../utils/utils";

const More: React.FC<MoreProps> = ({ more }) => {
  const [show, setShow] = React.useState(false);
  return (
    <div className={concatClassName([styles.more_con])}>
      <IoIosMore onClick={() => setShow(!show)} className="c-p" size={20} />
      {show && (
        <div className={concatClassName([styles.dropdown])}>
          {more?.map((data, ind) => {
            return (
              <button
                onClick={data.action}
                key={ind}
                className={concatClassName([styles.action])}
              >
                {data.icon}
                <p>{data.label}</p>
              </button>
            );
          })}
        </div>
      )}
    </div>
  );
};
export default More;
