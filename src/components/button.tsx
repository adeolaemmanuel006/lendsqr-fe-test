import { useNavigate } from "react-router-dom";
import { concatClassName } from "../utils/utils";
import styles from "../scss/component.module.scss";

const Button: React.FC<ButtonProps> = ({
  type,
  disable,
  to,
  isLoading,
  loadingText,
  children,
  className = "",
  onClick,
}) => {
  const navigate = useNavigate();
  return (
    <button
      type={type}
      disabled={disable}
      onClick={(e) => {
        if (!disable) {
          if (to) {
            navigate(to);
          } else {
            onClick?.(e);
          }
        }
      }}
      className={concatClassName([styles.button, className as string])}
    >
      {isLoading ? <>{/* {loadingText} <Loader /> */}</> : children}
    </button>
  );
};
export default Button;
