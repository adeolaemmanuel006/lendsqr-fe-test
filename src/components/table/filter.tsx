import { concatClassName } from "../../utils/utils";
import Input from "../input";
import styles from "../../scss/layout.module.scss";
import Button from "../button";
import { FaCalendarAlt } from "react-icons/fa";

const Filter: React.FC<FilterProps> = () => {
  return (
    <div className={concatClassName([styles.filter])}>
      <Input
        type={"select"}
        labelText="Organization"
        placeholder="Organization"
      />
      <Input
        type={"text"}
        labelText="Username"
        placeholder="Username"
        containerClass="mt-3"
      />
      <Input
        type={"text"}
        labelText="Email"
        placeholder="Email"
        containerClass="mt-3"
      />
      <Input
        type={"text"}
        labelText="Date"
        placeholder="Date"
        containerClass="mt-3"
        icon={<FaCalendarAlt className="mt-2 mr-2" />}
      />
      <Input
        type={"text"}
        labelText="Phone Number"
        placeholder="Phone Number"
        containerClass="mt-3"
      />
      <Input
        type={"select"}
        placeholder="Select"
        labelText="Status"
        containerClass="mt-3"
        options={[
          { label: "Failed" },
          { label: "Success" },
          { label: "Inactive" },
          { label: "Active" },
          { label: "Pending" },
        ]}
      />
      <div className={"flex js-b mt-3"}>
        <button className={"border_btn w-50"}>Reset</button>
        <Button className="w-50">Filter</Button>
      </div>
    </div>
  );
};
export default Filter;
