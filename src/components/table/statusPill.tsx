import React from "react";

const StatusPill: React.FC<StatusPillProps> = ({ type }) => {
  return (
    <React.Fragment>
      {type === "Active" && (
        <p
          className={`active_status fs-7`}
        >
          {type}
        </p>
      )}
      {type === "Inactive" && (
        <p
          className={`inactive_status fs-7`}
        >
          {type}
        </p>
      )}
      {type === "Pending" && (
        <p
          className={`pending_status fs-7`}
        >
          {type}
        </p>
      )}
      {type === "Blacklisted" && (
        <p
          className={`blacklisted_status fs-7`}
        >
          {type}
        </p>
      )}
    </React.Fragment>
  );
};
export default StatusPill;
