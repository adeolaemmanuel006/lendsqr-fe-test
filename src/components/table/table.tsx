import {
  //   Column,
  //   Table as ReactTable,
  useReactTable,
  getCoreRowModel,
  getFilteredRowModel,
  getPaginationRowModel,
  //   ColumnDef,
  //   OnChangeFn,
  flexRender,
} from "@tanstack/react-table";
import React from "react";
import { concatClassName, useGetScreen } from "../../utils/utils";
import styles from "../../scss/component.module.scss";
import Filter from "./filter";
import ReactPaginate from "react-paginate";
import Input from "../input";
import { BsChevronLeft, BsChevronRight } from "react-icons/bs";

const Table: React.FC<TableProps> = ({
  data,
  columns,
  tableHeader,
  className,
  onRowClick,
  paginationShow = false,
  exportShow = false,
  filterShow = false,
}) => {
  const { isMobilePortrait } = useGetScreen();
  const [limit, setLimit] = React.useState(10);
  const table = useReactTable({
    data,
    columns,
    // Pipeline
    getCoreRowModel: getCoreRowModel(),
    getFilteredRowModel: getFilteredRowModel(),
    getPaginationRowModel: getPaginationRowModel(),
    //
    // debugTable: true,
  });

  return (
    <React.Fragment>
      <div className={concatClassName([styles.table_con])}>
        <div className="relative">
          <table
            className={concatClassName([styles.table, className as string])}
          >
            <thead className={concatClassName([tableHeader as string])}>
              {table.getHeaderGroups().map((headerGroup) => (
                <tr key={headerGroup.id}>
                  {headerGroup.headers.map((header) => (
                    <th key={header.id} className="py-3 px-6">
                      {header.isPlaceholder
                        ? null
                        : flexRender(
                            header.column.columnDef.header,
                            header.getContext()
                          )}
                    </th>
                  ))}
                </tr>
              ))}
            </thead>
            <tbody>
              {table.getRowModel().rows.map((row) => (
                <tr
                  key={row.id}
                  className='class="bg-white border-b hover:bg-gray-100 cursor-pointer'
                  onClick={() => onRowClick?.({ ...row.original })}
                >
                  {row.getVisibleCells().map((cell) => (
                    <td key={cell.id}>
                      {flexRender(
                        cell.column.columnDef.cell,
                        cell.getContext()
                      )}
                    </td>
                  ))}
                </tr>
              ))}
            </tbody>
            {/* <tfoot>
          {table.getFooterGroups().map((footerGroup) => (
            <tr key={footerGroup.id}>
              {footerGroup.headers.map((header) => (
                <th key={header.id}>
                  {header.isPlaceholder
                    ? null
                    : flexRender(
                        header.column.columnDef.footer,
                        header.getContext()
                      )}
                </th>
              ))}
            </tr>
          ))}
        </tfoot> */}
          </table>
          {filterShow && <Filter />}
        </div>
      </div>
      <div className={`${isMobilePortrait ? "col" : "flex js-b"}`}>
        <div className={`${isMobilePortrait ? "m-auto flex mb-2" : "flex"}`}>
          <p className="mt-2">Showing</p>
          <Input
            type={"select"}
            defaultValue={10}
            onChange={(d) => {
              setLimit(d);
              table.setPageSize(d);
            }}
            options={[
              { label: "10", value: 10 },
              { label: "30", value: 30 },
              { label: "50", value: 50 },
              { label: "100", value: 100 },
            ]}
            className='bg-grey'
          />
          <p className="mt-2">out of {data.length}</p>
        </div>
        <div>
          <ReactPaginate
            pageCount={Math.ceil(data.length / limit)}
            breakLabel="..."
            previousLabel={
              <BsChevronLeft
                onClick={() => table.previousPage()}
                color="#84818A"
                className="pagination_btn"
              />
            }
            nextLabel={
              <BsChevronRight
                onClick={() => table.nextPage()}
                color="#84818A"
                className="pagination_btn"
              />
            }
            onPageChange={(data) => table.setPageIndex(data.selected)}
            containerClassName={"pagination"}
            previousLinkClassName={"pagination__link"}
            nextLinkClassName={"pagination__link"}
            disabledClassName={"pagination__link--disabled"}
            activeClassName={"pagination__link--active"}
            // forcePage={Math.ceil(pageSettings.skip / pageSettings.limit)}
          />
        </div>
      </div>
    </React.Fragment>
  );
};
export default Table;
