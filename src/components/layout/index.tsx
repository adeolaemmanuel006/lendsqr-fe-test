import React from "react";
import { useLocation } from "react-router-dom";
import { app_routes } from "../../utils/routes";
import AppLayout from "./appLayout";

const Layout: React.FC<LayoutProps> = ({ children }) => {
  const location = useLocation();
  const isAppLayout = app_routes.indexOf(location.pathname) !== -1;
  
  if (isAppLayout) return <AppLayout>{children}</AppLayout>;
  return <React.Fragment>{children}</React.Fragment>;
};
export default Layout;
