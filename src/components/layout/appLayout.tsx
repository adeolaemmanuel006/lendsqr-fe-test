import React from "react";
import { concatClassName, useGetScreen } from "../../utils/utils";
import styles from "../../scss/layout.module.scss";
import Logo from "../logo";
import Input from "../input";
import Button from "../button";
import { BsSearch, BsBell } from "react-icons/bs";
import { RiArrowDropDownLine } from "react-icons/ri";
import { Link } from "react-router-dom";
import icon from "../../assets/img/image";
import routes from "../../utils/routes";
import { IoMdArrowDropdown } from "react-icons/io";
import { HiBars3BottomLeft } from "react-icons/hi2";
import { useLocation } from "react-router-dom";

/**
 * Layout for main app
 */
const AppLayout: React.FC<AppLayoutProps> = ({ children }) => {
  const { isMobilePortrait } = useGetScreen();
  const location = useLocation();
  const [navShow, setNavShow] = React.useState(isMobilePortrait ? false : true);
  const aside_links = [
    { icon: icon.home, label: "Dashboard", to: "" },
    {
      label: "CUSTOMERS",
      children: [
        { icon: icon.user, label: "Users", to: routes.users },
        { icon: icon.users_2, label: "Guarantors", to: '' },
        { icon: icon.sack, label: "Loans", to: '' },
        { icon: icon.handshake, label: "Decision Models", to: '' },
        { icon: icon.piggy_bank, label: "Savings", to: '' },
        { icon: icon.loan_request, label: "Loan Requests", to: '' },
        { icon: icon.user_check, label: "Whitelist", to: '' },
        { icon: icon.user_times, label: "Karma", to: '' },
      ],
    },
    {
      label: "BUSINESSES",
      children: [
        { icon: icon.briefcase, label: "Organization", to: '' },
        { icon: icon.loan_request, label: "Loan Products", to: '' },
        { icon: icon.savings, label: "Savings Products", to: '' },
        { icon: icon.coins_solid, label: "Fees and Charges", to: '' },
        { icon: icon.transaction, label: "Transactions", to: '' },
        { icon: icon.galaxy, label: "Services", to: '' },
        { icon: icon.user_cog, label: "Service Account", to: '' },
        { icon: icon.scroll, label: "Settlements", to: '' },
        { icon: icon.chart_bar, label: "Reports", to: '' },
      ],
    },
    {
      label: "SETTINGS",
      children: [
        { icon: icon.sliders, label: "Preferences", to: '' },
        {
          icon: icon.badge_percent,
          label: "Fees and Pricing",
          to: '',
        },
        { icon: icon.clipboard_list, label: "Audit Logs", to: '' },
      ],
    },
  ];

  const active_nav = (to: string) => {
    if (to === location.pathname) return "active-side_link";
    else return "";
  };

  return (
    <div className={concatClassName([styles.app_layout_con])}>
      <nav>
        <div className={concatClassName([styles.nav_container])}>
          <div className={concatClassName([styles.nav_right])}>
            {isMobilePortrait && (
              <HiBars3BottomLeft
                onClick={() => setNavShow(!navShow)}
                size={35}
                className="c-p"
              />
            )}
            <Logo
              logoClass={concatClassName(["w-10"])}
              logoTextClass="w-50"
              className={styles.logo_con}
            />
            <Input
              type={"text"}
              placeholder="Search for anything"
              containerClass={styles.input}
              icon={
                <Button className={styles.btn}>
                  <BsSearch />
                </Button>
              }
            />
          </div>
          <div className={concatClassName([styles.nav_left])}>
            <Link to={""} className={concatClassName([styles.nav_link])}>
              Docs
            </Link>
            <BsBell className={concatClassName([styles.icon_bell])} size={23} />
            <div className={concatClassName([styles.user_nav])}>
              <img
                src={icon.user_avatar}
                alt="user avatar"
                className={concatClassName([styles.img, "mt-1"])}
              />
              <p className={concatClassName([styles.text])}>Adedeji</p>
              <IoMdArrowDropdown className={concatClassName([styles.text])} />
            </div>
          </div>
        </div>
      </nav>
      <div className={concatClassName([styles.main_con])}>
        {(!isMobilePortrait || navShow) && (
          <aside className="animate__fadeInLeft">
            <div className={styles.org_con}>
              <button className={concatClassName([styles.btn])}>
                <img src={icon.briefcase} alt="briefcase" />
                <p className={concatClassName([styles.text])}>
                  Switch Organization
                </p>
                <RiArrowDropDownLine size={30} className="theme-text" />
              </button>
            </div>

            <div className={styles.aside_route}>
              {aside_links.map((data, ind) => {
                return (
                  <React.Fragment key={`par-${ind}`}>
                    {data.children && (
                      <React.Fragment>
                        <p className={concatClassName([styles.child_name])}>
                          {data.label}
                        </p>
                        {data.children.map((data, key) => {
                          return (
                            <Link
                              to={data.to}
                              key={`child-${key}`}
                              className={concatClassName([
                                styles.btn,
                                active_nav(data.to),
                              ])}
                              style={{ paddingTop: 16 }}
                            >
                              <img src={data.icon} alt={data.label} />
                              <p className={concatClassName([styles.text])}>
                                {data.label}
                              </p>
                            </Link>
                          );
                        })}
                      </React.Fragment>
                    )}
                    {!data.children && (
                      <Link
                        to={data.to}
                        className={concatClassName([styles.btn, ""])}
                      >
                        <img src={data.icon} alt={data.label} />
                        <p className={concatClassName([styles.text])}>
                          {data.label}
                        </p>
                      </Link>
                    )}
                  </React.Fragment>
                );
              })}
            </div>
          </aside>
        )}
        <main>
          <div className={styles.container}>{children}</div>
        </main>
      </div>
    </div>
  );
};
export default AppLayout;
