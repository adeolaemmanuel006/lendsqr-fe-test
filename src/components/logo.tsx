import React from "react";
import styles from "../scss/component.module.scss";
import icon from "../assets/img/image";
import { concatClassName } from "../utils/utils";

const Logo: React.FC<LogoProps> = ({
  className = "",
  logoClass = "",
  logoTextClass = "",
}) => {
  return (
    <div className={concatClassName([styles.logo_con, className as string])}>
      <img
        src={icon.logo}
        alt="logo"
        className={concatClassName([styles.img_logo, logoClass as string])}
      />
      <img
        src={icon.logoText}
        alt="logo"
        className={concatClassName([
          styles.img_logo_text,
          logoTextClass as string,
        ])}
      />
    </div>
  );
};
export default Logo;
