import React from "react";
import PasswordInput from "./passwordInput";
import SelectInput from "./selectInput";
import StringInput from "./stringInput";

const Input: React.FC<InputProps> = (props) => {
  return (
    <React.Fragment>
      {props.type === "text" && <StringInput {...props} />}
      {props.type === "password" && <PasswordInput {...props} />}
      {props.type === "select" && <SelectInput {...props} />}
    </React.Fragment>
  );
};
export default Input;
