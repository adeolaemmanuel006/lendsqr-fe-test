import React from "react";
import { concatClassName } from "../../utils/utils";
import styles from "../../scss/component.module.scss";

const SelectInput: React.FC<InputProps> = ({
  options,
  className,
  containerClass,
  labelText,
  id,
  inputClass,
  placeholder,
  onChange,
  defaultValue = "Selected",
}) => {
  return (
    <div
      className={concatClassName([
        styles.input_con,
        containerClass as string,
        inputClass as string,
      ])}
    >
      {labelText && <label htmlFor={id}>{labelText}</label>}
      <div className={concatClassName([styles.input, className as string])}>
        <select
          className={concatClassName([className as string, "w-100 select"])}
          placeholder={placeholder}
          onChange={(d) => onChange?.(d.target.value)}
          defaultValue={defaultValue}
        >
          <option value={defaultValue} disabled>
            {defaultValue}
          </option>
          {options?.map((data, ind) => {
            return (
              <option key={ind} value={data.value}>
                {data.label}
              </option>
            );
          })}
        </select>
      </div>
    </div>
  );
};
export default SelectInput;
