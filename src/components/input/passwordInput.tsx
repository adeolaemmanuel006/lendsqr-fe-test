import React from "react";
import StringInput from "./stringInput";

const PasswordInput: React.FC<InputProps> = (props) => {
  const [show, setShow] = React.useState("SHOW");
  const [type, setType] = React.useState(props.type);

  const togglePassword = () => {
    if (type === "password") {
      setShow("HIDE");
      setType("text");
    }
    if (type === "text") {
      setShow("SHOW");
      setType("password");
    }
  };

  return (
    <StringInput
      {...props}
      type={type}
      icon={
        <p className="theme-text-green fw-500 c-p fs-1 mt-2 mr-2" onClick={togglePassword}>{show}</p>
      }
    />
  );
};
export default PasswordInput;
