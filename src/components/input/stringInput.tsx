import React from "react";
import { concatClassName } from "../../utils/utils";
import styles from "../../scss/component.module.scss";

const StringInput: React.FC<InputProps> = ({
  type,
  id,
  className = "",
  labelText,
  inputClass = "",
  placeholder,
  icon,
  iconRight,
  iconLeft,
  containerClass = "",
}) => {
  const icon_row =
    (!iconRight || iconRight) && !iconLeft
      ? styles.icon_right
      : styles.icon_left;
  return (
    <div
      className={concatClassName([styles.input_con, containerClass as string])}
    >
      {labelText && <label htmlFor={id}>{labelText}</label>}
      <div
        className={concatClassName([
          styles.input,
          icon_row,
          className as string,
        ])}
      >
        <input
          className={inputClass}
          id={id}
          type={type}
          placeholder={placeholder}
        />
        {(!iconRight || iconRight) && !iconLeft && icon && icon}
      </div>
    </div>
  );
};

export default StringInput;
