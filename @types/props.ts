// import { SetStateAction } from "jotai";
import React, { CSSProperties, HTMLInputTypeAttribute } from "react";
import { NavigateFunction, Location } from "react-router";

declare global {
  export type Status =
    | "Active"
    | "Blacklisted"
    | "Pending"
    | "Inactive";
  interface Components {
    id?: string;
    children?: React.ReactNode;
    className?: string;
    style?: CSSProperties;
    onClick?: (data?: any) => void;
    location?: Location;
    navigate?: NavigateFunction;
  }
  type LayoutProps = Components & {};
  type UserProps = Components & {};
  type UserDetailsProps = Components & {};
  type StatusPillProps = Components & {
    type: Status;
  };
  type FilterProps = Components & {
    onChange?: (data?: any) => void;
  };
  type MoreProps = Components & {
    more?: {
      label?: string;
      to?: string;
      action?: (data?: any) => void;
      icon?: any;
    }[];
  };
  type LogoProps = Components & {
    logoClass?: string;
    logoTextClass?: string;
  };
  type AppLayoutProps = Components & {};
  type LoginProps = Components & {};
  export type InputProps = Components & {
    labelText?: string;
    type?: HTMLInputTypeAttribute | "select" | "textarea" | "switch";
    placeholder?: string;
    options?: {
      name?: string;
      label?: string;
      value?: string | number;
      img?: any;
    }[];
    onChange?: (data?: any) => void;
    selectInputChange?: (data?: any) => void;
    value?: any;
    labelClass?: string;
    required?: boolean;
    name?: string;
    maxLength?: number;
    disabled?: boolean;
    formatNumber?: string;
    onFocus?: (e: React.FocusEvent<HTMLInputElement, Element>) => void;
    onBlur?: (e: React.FocusEvent<HTMLInputElement, Element>) => void;
    inputClass?: string;
    iconLeft?: boolean;
    iconRight?: boolean;
    props?: any;
    icon?: any;
    containerClass?: string;
    defaultValue?: any
  };

  export type ButtonProps = Components & {
    isLoading?: boolean;
    loadingText?: string;
    type?: "button" | "submit" | "reset";
    disable?: boolean;
    to?: string;
    theme?: "#111727" | "white" | "plain" | "#888b93";
  };

  export type TableProps = Components & {
    data: any[];
    columns: any;
    headerClass?: string;
    tableHeader?: string;
    onRowClick?: (e?: any) => void;
    paginationShow?: boolean;
    exportShow?: boolean;
    filterShow?: boolean;
  };
}
