export {};
declare global {
  type UserDto = {
    createdAt: string;
    orgName: string;
    userName: string;
    email: string;
    phoneNumber: string;
    lastActiveDate: string;
    profile: {
      firstName: string;
      lastName: string;
      phoneNumber: string;
      avatar: string;
      gender: string;
      bvn: string;
      address: string;
      currency: "NGN";
    };
    guarantor: {
      firstName: string;
      lastName: string;
      phoneNumber: string;
      gender: string;
      address: string;
    };
    accountBalance: string;
    accountNumber: string;
    socials: {
      facebook: string;
      instagram: string;
      twitter: string;
    };
    education: {
      level: string;
      employmentStatus: string;
      sector: string;
      duration: string;
      officeEmail: string;
      monthlyIncome: string[];
      loanRepayment: string;
    };
    id: string;
  };

  type UserColumnDto = {
    ORGANIZATION: string;
    USERNAME: string;
    EMAIL: string;
    "PHONE NUMBER": string;
    "DATE JOINED": any;
    STATUS: any;
    id?: string;
  };

  type StorageData = UserDto;

  type StoreObject = { [x: string]: string };
}
